const { src, dest, parallel, series, watch } = require('gulp');
const gulp = require('gulp'),
  sass = require('gulp-sass')(require('sass'));
(rename = require('gulp-rename')),
  (csso = require('gulp-csso')),
  (aliases = require('gulp-style-aliases')),
  (concat = require('gulp-concat')),
  (imagemin = require('gulp-imagemin')),
  (webp = require('imagemin-webp')),
  (imageminSvgo = require('imagemin-svgo')),
  (uglify = require('gulp-uglify'));

const { getInstalledPathSync } = require('get-installed-path');
const bootstrapFolder = getInstalledPathSync('bootstrap', { local: true });
const sourceSass = ['./src/scss/main.scss'];

function sassTask() {
  return gulp
    .src(sourceSass)
    .pipe(
      aliases({
        '@bootstrap': bootstrapFolder,
      }),
    )
    .pipe(sass())
    .pipe(csso())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('../assets/dist/'));
}

function jsTask() {
  return gulp
    .src('./src/js/**/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(dest('./dist/js/'));
}

// JS Plugins
function pluginsTask() {
  return gulp
    .src([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/owl.carousel/dist/owl.carousel.min.js',
      'node_modules/wowjs/dist/wow.min.js',
      'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
      'node_modules/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js',
      'node_modules/@popperjs/core/dist/umd/popper.js',
      'node_modules/bootstrap/dist/js/bootstrap.min.js',
    ])
    .pipe(concat('plugins.js'))
    .pipe(dest('./dist/js/'));
}

function imagesTask() {
  return gulp
    .src('./src/img/**')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.mozjpeg({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({ plugins: [{ removeViewBox: false }] }),
      ]),
    )
    .pipe(dest('./dist/img/'));
}

// Watch files
function watchFiles() {
  watch('./src/scss/**/*.scss', parallel(sassTask));
  watch('./src/js/**/*.js', parallel(jsTask));
  watch('js/plugins/*.js', parallel(pluginsTask));
}

exports.default = parallel(sassTask, jsTask, imagesTask, watchFiles);
exports.build = series(sassTask, jsTask, pluginsTask, imagesTask);
