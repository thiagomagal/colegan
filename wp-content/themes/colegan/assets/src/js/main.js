$ = jQuery;

$('document').ready(function () {
  var top = 100;

  $(window).scroll(function (event) {
    var current = $(this).scrollTop();
    if (current > top) {
      $('header').addClass('reduced');
    } else {
      $('header').removeClass('reduced');
    }
  });

  $('.icon-menu-mobile').click(function () {
    $(this).toggleClass('close');
    $('header').toggleClass('open');
  });

  $('#main-menu--mobile a').click(function () {
    $('.icon-menu-mobile').removeClass('close');
    $('header').removeClass('open');
  });

  var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 160, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true, // act on asynchronously loaded content (default is true)
    scrollContainer: null, // optional scroll container selector, otherwise use window
    callback: function (box) {
      if (box.id == 'number-1') startCounters();
    },
  });
  wow.init();

  $('.scroll-to').click(function () {
    var element = $(this).data('element');
    scrollTo(element);
  });

  var scrollSpy = new bootstrap.ScrollSpy(document.body, {
    target: '#main-menu',
    offset: 100,
  });

  $('.highlights-images').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    autoplay: true,
    autoplayTimeout: 2500,
    dots: false,
    items: 1,
  });

  $('input[name=telefone]')
    .mask('(99) 9999-9999?9')
    .focusout(function (event) {
      var target, phone, element;
      target = event.currentTarget ? event.currentTarget : event.srcElement;
      phone = target.value.replace(/\D/g, '');
      element = $(target);
      element.unmask();
      if (phone.length > 10) {
        element.mask('(99) 99999-999?9');
      } else {
        element.mask('(99) 9999-9999?9');
      }
    });

  window.initial_msg = $('textarea[name="mensagem"]').val();
  $('textarea[name="mensagem"]').focusin(function () {
    if ($(this).val() == window.initial_msg) $(this).val('');
  });
  $('textarea[name="mensagem"]').focusout(function () {
    if ($(this).val() == '') $(this).val(window.initial_msg);
  });

  document.addEventListener(
    'wpcf7mailsent',
    function (event) {
      if ($('.file-open').length) window.open($('.file-open').val(), '_blank');
    },
    false,
  );
});

function is_mobile() {
  if ($(window).width() <= 768) return true;
  else return false;
}

function scrollTo(element) {
  $('html, body').animate({ scrollTop: $(element).offset().top }, 1000);
}

function startCounters() {
  var counters = $('.counter');
  var countersQuantity = counters.length;
  var counter = [];

  for (i = 0; i < countersQuantity; i++) {
    counter[i] = parseInt(counters[i].innerHTML);
  }

  var count = function (start, value, id) {
    var localStart = start;
    var speed = 15 - (value * 4) / 200;

    setInterval(function () {
      if (localStart < value) {
        localStart++;
        counters[id].innerHTML = localStart;
      }
    }, speed);
  };

  for (j = 0; j < countersQuantity; j++) {
    count(0, counter[j], j);
  }
}

function sendToWhatsapp() {
  var nome = $('input[name=nome]').val(),
    email = $('input[name=email]').val(),
    telefone = $('input[name=telefone]').val(),
    tipo = $('select[name=tipo_uniforme]').val(),
    msg = $('textarea[name=mensagem]').val();
  if (nome && email && telefone && tipo && msg != window.initial_msg) {
    var text = 'Nome: ' + nome + '\n';
    text += 'E-mail: ' + email + '\n';
    text += 'Telefone: ' + telefone + '\n';
    text += 'Tipo de uniforme: ' + tipo + '\n';
    text += 'Mensagem: ' + msg + '\n';
    console.log(text);
    window.open(
      'https://api.whatsapp.com/send?phone=553134520671&text=' +
        encodeURI(text),
      '_blank',
    );
  } else {
    $('.home-budget form')
      .removeClass('init')
      .addClass('invalid')
      .data('status', 'invalid');
    $('.home-budget .wpcf7-response-output').html(
      'Preencha todos os campos. Verifique e tente novamente.',
    );
  }
}
