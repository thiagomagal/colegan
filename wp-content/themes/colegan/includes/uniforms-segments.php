<?php $uniforms = get_posts(array('post_type' => 'uniform')); ?>
<section>
  <div class="container-fluid">
    <div class="row">
      <?php $c = 1;
      foreach ($uniforms as $uniform) :   $image = get_the_post_thumbnail_url($uniform->ID, 'large');
      ?>
        <div class="col-md-6 bg-color-<?php echo $c; ?> color-white  px-0 text-center">
          <a class="home-uniform" href="<?php the_permalink($uniform->ID); ?>" <?php if ($image) : ?> style="background-image:url(<?php echo $image; ?>);" <?php endif; ?>>
            <h3>Uniformes <br><?php echo $uniform->post_title; ?></h3>
          </a>
        </div>
      <?php $c++;
      endforeach; ?>
    </div>
  </div>
</section>