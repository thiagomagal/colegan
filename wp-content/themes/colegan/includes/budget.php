<section class="section-y bg-color-1 color-white text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1 mb-5">
        <h2>
          Para um orçamento mais preciso, considere os itens abaixo:</h2>
      </div>
      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
        <div class="round-icon bg-color-3">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 455 455" style="enable-background:new 0 0 455 455;" xml:space="preserve">
            <path d="M357.642,32.638l-66.814-15.896c0,0-18.622,32.784-63.328,32.784s-63.328-32.784-63.328-32.784L97.358,32.638L0,119.916 l48.555,75.179l48.803-22.87v266.032h260.284V172.225l48.803,22.87L455,119.916L357.642,32.638z" />
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
          </svg>
        </div>
        <h3>Tipo de Camisa e Tecido</h3>
        <p>O primeiro passo é definir o tipo de camisa que sua empresa precisa, levando em conta o tipo de tecido, a cor, a gola e os punhos.</p>
      </div>
      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
        <div class="round-icon bg-color-3">
          <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="265.192px" height="265.192px" viewBox="0 0 265.192 265.192" style="enable-background:new 0 0 265.192 265.192;" xml:space="preserve">
            <g>
              <g>
                <path d="M252.192,0H13C5.835,0,0,5.835,0,13v239.192c0,7.166,5.835,13,13,13h239.192c7.171,0,13-5.834,13-13V13 C265.192,5.835,259.363,0,252.192,0z M249.593,249.593H15.6V15.6h233.993V249.593z" />
                <path d="M40.408,236.593h184.373c6.511,0,11.812-5.301,11.812-11.811V40.408c0-6.51-5.301-11.809-11.812-11.809H40.408 c-6.51,0-11.809,5.299-11.809,11.809v184.374C28.599,231.292,33.898,236.593,40.408,236.593z M44.199,44.199h176.795v176.795 H44.199V44.199z" />
                <path d="M150.42,111.043c0-0.411,0-0.609-0.406-0.609c-1.62,0-34.441,4.659-45.377,5.472c-0.208,5.469-0.406,11.75-0.406,17.219 l12.964,0.609v49.836c0,0.406-0.208,0.812-0.808,1.016l-10.737,0.406c-0.208,5.261-0.208,11.339-0.208,16.818 c8.305,0,19.042-0.214,27.555-0.214c8.709,0,20.257,0,27.954,0c0-5.469,0-11.141,0-16.813l-11.141-0.406 C149.811,164.12,150.227,132.911,150.42,111.043z" />
                <path d="M130.776,98.48c10.128,0,19.035-7.299,19.035-18.235c0-8.514-6.276-16.407-18.024-16.407 c-11.144,0-18.631,8.305-18.631,16.811C113.155,90.982,122.062,98.48,130.776,98.48z" />
              </g>
            </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
            <g> </g>
          </svg>
        </div>
        <h3>Logos e Detalhes​</h3>
        <p>Definir o logo a ser estampado, assim como detalhes. Se serão bordados ou silcados.</p>
      </div>
      <div class="col-md-4 wow fadeInUp" data-wow-delay="0.3s">
        <div class="round-icon bg-color-3">
          <svg width="256px" height="256px" viewBox="0 0 256 256" id="Flat" xmlns="http://www.w3.org/2000/svg">
            <path d="M72.01074,170.625,55.99219,192H68a8,8,0,0,1,0,16H40a8.00013,8.00013,0,0,1-6.40186-12.79785l25.46729-33.9834A6.00232,6.00232,0,1,0,48.46826,155.667a8,8,0,1,1-14.73633-6.23242,22.00229,22.00229,0,1,1,38.50635,20.87109Q72.12842,170.46826,72.01074,170.625ZM43.57764,67.15527,48,64.94434v43.0498a8,8,0,1,0,16,0V52a7.99928,7.99928,0,0,0-11.57764-7.15527l-16,8a7.99984,7.99984,0,1,0,7.15528,14.31054ZM104,72H215.999a8,8,0,0,0,0-16H104a8,8,0,0,0,0,16ZM215.999,184h-112a8,8,0,1,0,0,16h112a8,8,0,0,0,0-16Zm0-64H104a8,8,0,0,0,0,16H215.999a8,8,0,0,0,0-16Z" />
          </svg>

        </div>
        <h3>Quantidade</h3>
        <p>Uma lista de tamanhos a serem produzidos complementa as informações iniciais necessárias para o seu orçamento. Produzimos a partir de 10 peças de um mesmo modelo. </p>
      </div>
      <div class="col-12 text-center pt-5">
        <a class="btn white" href="#orcamento">Solicitar Orçamento</a>
      </div>
    </div>
  </div>
</section>

<section id="orcamento" class="home-budget">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <h1>Orçamento</h1>
        <?php echo do_shortcode('[contact-form-7  title="Orçamento"]') ?>
        <p>OU</p>
        <a href="javascript:sendToWhatsapp();" class="btn btn-submit">Enviar por whatsapp</a>
      </div>
    </div>
  </div>
</section>