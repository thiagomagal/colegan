<section data-wow-delay="0.5s" class="py-5 block <?php echo $block['acf_fc_layout']; ?>">
  <div class="container">
    <div class="row">
      <div class="col-md-4 offset-md-1 align-self-center me-4 me-md-0">
        <h2><?php echo $block['titulo']; ?></h2>
      </div>
      <div class="col-md-6 align-self-center">
        <?php echo $block['texto']; ?>
      </div>
    </div>
  </div>
</section>