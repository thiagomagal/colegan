<?php
$items = $block['itens'];
$c = $block['cor'];
if ($items) :
  foreach ($items as $item) :
    $image = $item['imagem']['url'];
?>
    <section data-wow-delay="0.5s" class="block <?php echo $block['acf_fc_layout']; ?>  <?php echo ($c % 2 == 0) ? 'white' : 'blue'; ?> <?php echo ($item['imagem']['height'] > $item['imagem']['width']) ? 'vertical-image' : ''; ?>">
      <div class="container">
        <div class="row flex-column-reverse flex-md-row">
          <div class="<?php echo $block['acf_fc_layout']; ?>_box align-self-center wow fadeIn ">
            <h2><?php echo $item['nome'] ?></h2>
            <div>
              <?php echo $item['descricao'] ?>
            </div>
          </div>
          <div class="col-md-12 mb-5 mb-md-0  wow fadeInRight text-end">
            <img class="img-fluid" src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
          </div>
        </div>
      </div>
    </section>

<?php $c++;
  endforeach;
endif;
?>