<?php $cases = $block['cases'];
if ($cases) : foreach ($cases as $case) :
    $image = get_the_post_thumbnail_url($case->ID, 'large'); ?>
    <section id="<?php echo $case->post_name; ?>" data-wow-delay="0.5s" class="cases-item block wow fadeInUp">
      <div class="container">
        <div class="row flex-column-reverse flex-md-row">
          <div class="col-md-6 pe-md-5 align-self-center wow fadeIn">
            <h2><br><?php echo $case->post_title; ?></h2>
            <div>
              <?php echo get_the_excerpt($case->ID);  ?>
              <br>
              <a href="<?php echo BLOG_URL ?>/clientes/#<?php echo $case->post_name; ?>" class="btn mt-3">Saiba mais</a>
            </div>
          </div>
          <div class="col-md-6 mb-3 mb-md-0 align-self-center">
            <img class="img-fluid" src="<?php echo $image; ?>" alt="<?php the_title(); ?>">
          </div>
        </div>
      </div>
    </section>
<?php endforeach;
endif; ?>