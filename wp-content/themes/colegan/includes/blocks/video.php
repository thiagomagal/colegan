<?php $code = getYoutubeIdFromUrl($block['url_video']);
if ($code) : ?>
  <section class="block-video block <?php echo $block['acf_fc_layout']; ?>">
    <div class="container">
      <div class="row">
        <div class="col-6 col-md-3  align-self-center">
          <h2><?php echo $block['titulo']; ?></h2>
        </div>
      </div>
    </div>
    <a data-youtube="<?php echo $code; ?>" class="block-video_video">
      <?php if ($block['imagem']) : ?>
        <img src="<?php echo $block['imagem']; ?>" alt="<?php echo $block['titulo']; ?>" />
      <?php else : ?>
        <img src="https://img.youtube.com/vi/<?php echo $code; ?>/hqdefault.jpg" alt="<?php echo $block['titulo']; ?>" />
      <?php endif; ?>
      <svg width="154" height="154" viewBox="0 0 154 154" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_d_467_4265)">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M62.833 104.625L102.333 75L62.833 45.375V104.625ZM75.9998 9.16699C39.6598 9.16699 10.1665 38.6603 10.1665 75.0003C10.1665 111.34 39.6598 140.834 75.9998 140.834C112.34 140.834 141.833 111.34 141.833 75.0003C141.833 38.6603 112.34 9.16699 75.9998 9.16699ZM75.9997 127.667C46.9672 127.667 23.333 104.033 23.333 75.0006C23.333 45.9682 46.9672 22.334 75.9997 22.334C105.032 22.334 128.666 45.9682 128.666 75.0006C128.666 104.033 105.032 127.667 75.9997 127.667Z" fill="white" />
          <path d="M62.833 104.625H62.333V105.625L63.133 105.025L62.833 104.625ZM102.333 75L102.633 75.4L103.166 75L102.633 74.6L102.333 75ZM62.833 45.375L63.133 44.975L62.333 44.375V45.375H62.833ZM63.133 105.025L102.633 75.4L102.033 74.6L62.533 104.225L63.133 105.025ZM102.633 74.6L63.133 44.975L62.533 45.775L102.033 75.4L102.633 74.6ZM62.333 45.375V104.625H63.333V45.375H62.333ZM75.9998 8.66699C39.3837 8.66699 9.6665 38.3842 9.6665 75.0003H10.6665C10.6665 38.9365 39.936 9.66699 75.9998 9.66699V8.66699ZM9.6665 75.0003C9.6665 111.616 39.3837 141.334 75.9998 141.334V140.334C39.936 140.334 10.6665 111.064 10.6665 75.0003H9.6665ZM75.9998 141.334C112.616 141.334 142.333 111.616 142.333 75.0003H141.333C141.333 111.064 112.064 140.334 75.9998 140.334V141.334ZM142.333 75.0003C142.333 38.3842 112.616 8.66699 75.9998 8.66699V9.66699C112.064 9.66699 141.333 38.9365 141.333 75.0003H142.333ZM75.9997 127.167C47.2433 127.167 23.833 103.757 23.833 75.0006H22.833C22.833 104.309 46.691 128.167 75.9997 128.167V127.167ZM23.833 75.0006C23.833 46.2443 47.2433 22.834 75.9997 22.834V21.834C46.691 21.834 22.833 45.692 22.833 75.0006H23.833ZM75.9997 22.834C104.756 22.834 128.166 46.2443 128.166 75.0006H129.166C129.166 45.692 105.308 21.834 75.9997 21.834V22.834ZM128.166 75.0006C128.166 103.757 104.756 127.167 75.9997 127.167V128.167C105.308 128.167 129.166 104.309 129.166 75.0006H128.166Z" fill="#F6F6F6" />
        </g>
        <defs>
          <filter id="filter0_d_467_4265" x="0.666504" y="0.666992" width="152.667" height="152.667" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
            <feFlood flood-opacity="0" result="BackgroundImageFix" />
            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
            <feOffset dx="1" dy="2" />
            <feGaussianBlur stdDeviation="5" />
            <feComposite in2="hardAlpha" operator="out" />
            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_467_4265" />
            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_467_4265" result="shape" />
          </filter>
        </defs>
      </svg>

    </a>
  </section>

  <div id="video" class="block-video_modal modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <iframe frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
<?php endif; ?>