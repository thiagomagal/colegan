<section data-wow-delay="0.5s" class="py-5 block <?php echo $block['acf_fc_layout']; ?>">
  <div class="container">
    <div class="row">
      <div class="col-md-5 offset-md-1 align-self-center me-4 me-md-0">
        <h2><?php echo $block['titulo']; ?></h2>
        <?php echo nl2br($block['texto']); ?>
      </div>
      <div class="col-md-5 align-self-center">
        <img class="img-fluid" src="<?php echo $block['imagem']; ?>" alt="<?php echo $block['titulo']; ?>">
      </div>
    </div>
  </div>
</section>