<?php $reasons = $block['motivos'];
$c = 1;
if ($reasons) : ?>
  <section class="block <?php echo $block['acf_fc_layout']; ?>">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h2>Porque nos contratar</h2>
        </div>
      </div>
      <div class="row justify-content-center">
        <?php foreach ($reasons as $reason) :  ?>
          <div class="col-md text-center mb-4  wow fadeInUp" data-wow-delay="0.<?php echo $c; ?>s">
            <div class="porque_contratar_item">
              <?php echo the_acf_icon($reason['icone']); ?>
              <h3><?php echo $reason['titulo'] ?></h3>
              <p><?php echo $reason['descricao'] ?></p>
            </div>
          </div>
        <?php $c++;
        endforeach; ?>
      </div>
    </div>
  </section>
<?php endif; ?>