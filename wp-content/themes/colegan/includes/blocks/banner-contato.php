<section class="banner-contact  block <?php echo $block['acf_fc_layout']; ?>" <?php if ($block['imagem']) : ?> style="background-image:url(<?php echo $block['imagem']; ?>);" <?php endif; ?>>
  <div class="container">
    <div class="row">
      <div class="col-md-6 wow fadeIn">
        <h2><?php echo $block['titulo'] ?></h2>
        <a class="btn" href="<?php echo ($t = $block['link']) ? $t : BLOG_URL . '/contato'; ?>"><?php echo ($t = $block['texto_botao']) ? $t : 'Entrar em contato'; ?></a>
      </div>
    </div>
  </div>
</section>