<?php get_header(); ?>

<?php 

if(have_posts()): the_post();
	$categories = get_the_category(get_the_ID());
	$image = get_the_post_thumbnail_url(get_the_ID(), 'large');

	if (has_category('receita',$post->ID)):
?>

<section data-wow-delay="0.5s" class="pb-5 post-single">
	<div class="container">
		<div class="row">
			<div class="col-md-6 mb-5 wow fadeInUp mb-5 post-single__highlight">
				<?php if($image): ?>
					<img class="img-fluid w-100" src="<?php echo $image; ?>" class="img-fluid" />
				<?php endif; ?>
			</div>	
			<div class="col-md-6 mb-5 wow fadeInUp mb-5 post-single__highlight align-self-center">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>	
			<?php if($ingredients = get_field('ingredientes')): ?>
			<div class="col-sm-8 offset-md-2 ingredient-item align-self-center wow fadeInUp mb-5">
				<div class="mb-3">
					<h2 class="mb-4 wow fadeInLeft">Ingredientes</h2>
					<?php echo $ingredients; ?>
				</div>									
			</div>	
			<?php endif; ?>

			<?php if($steps = get_field('modo_de_preparo')):  ?>
			<div class="col-sm-8 offset-md-2 align-self-center mb-5">
				<div class="row">
					<div class="col-12">
						<h2 class="mb-4 wow fadeInLeft">Mode de preparo</h2>
					</div>					
					<?php $c = 1; foreach($steps as $step): ?>
						<div class="col-md-12 py-4 wow fadeInUp step-item" data-wow-delay="0.<?php echo $c; ?>s">
							<div class="row">
								<div class="col-auto px-5 align-self-start text-center bg-color-1 py-4">
									<b class="font-size-30 color-white"><?php echo $c ?> </b>
								</div>
								<div class="col align-self-center">
									<?php echo nl2br($step['descricao']); ?>
								</div>
							</div>
						</div>	
					<?php $c++; endforeach;  ?>
				</div>								
			</div>	
			<?php endif; ?>

			<?php if($blocks = get_field('bloco_adicional')): $c = 1; foreach($blocks as $block): ?>
			<div class="col-sm-10 offset-md-1 align-self-center wow fadeInUp mb-5">
				<div class="mb-3">
					<h2 class="mb-4 wow fadeInLeft"><?php echo $block['titulo']; ?></h2>
					<?php echo $block['conteudo']; ?>
				</div>									
			</div>	
			<?php $c++; endforeach;  endif; ?>						

			<div class="col-sm-10 offset-md-1 align-self-center wow">
				<div class="mb-3">
					<?php echo get_list_taxonomy(get_the_ID(), 'category', true, ', ', 'font-size-08 underline italic color-black') ?>
				</div>									
			</div>							
		</div>
	</div>
</section>



<?php
	else:
?>

<section data-wow-delay="0.5s" class="pb-5 post-single">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-5 wow fadeInUp mb-5 post-single__highlight">
				<?php if($image): ?>
					<img class="img-fluid w-100" src="<?php echo $image; ?>" class="img-fluid" />
				<?php endif; ?>
			</div>	
			<div class="col-sm-10 offset-md-1 align-self-center wow">
				<h1><?php the_title(); ?></h1>
				<p class="italic"><?php the_date(); ?></p>
				<?php the_content(); ?>
				<div class="mb-3">
					<?php echo get_list_taxonomy(get_the_ID(), 'category', true, ', ', 'font-size-08 underline italic color-black') ?>
				</div>									
			</div>							
		</div>
	</div>
</section>

<?php endif; ?>

<section>
	<div class="container py-5">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="d-inline-block mb-4">Compartilhar</h3> <br>
				<?php echo do_shortcode('[Sassy_Social_Share]'); ?>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="container py-5">
		<div class="row">
			<div class="col-12 text-center mb-4">
				<h3 class="d-inline-block">Deixe seu comentário</h3>
			</div>
			<div class="col-md-8 offset-md-2 text-center">
				<div class="fb-comments" data-href="<?php the_permalink(); ?>" width="100%" data-numposts="5"></div>		
			</div>			
		</div>
	</div>
</section>

<?php endif; ?>

<?php get_footer(); ?>