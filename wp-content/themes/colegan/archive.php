<?php get_header(); ?>

<?php if($current_category = single_cat_title("", false)){ $cat ?>

	<section>
		<div class="container pt-5">
			<div class="row">
				<div class="col">
					<h3 class="thin l-spacing-3">Resultados para "<?php echo $current_category ?>": </h3>
				</div>
			</div>
		</div>
	</section>

<?php } ?>

<?php get_template_part('loop', 'loop'); ?>

<?php get_footer(); ?>