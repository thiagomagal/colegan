<?php

if (!function_exists('assets')) {
	function assets()
	{
		$the_theme = wp_get_theme();
		wp_enqueue_style('styles', get_stylesheet_directory_uri() . '/assets/dist/main.min.css', array(), $the_theme->get('Version'), false);
		wp_enqueue_script('plugins', get_template_directory_uri() . '/assets/dist/js/plugins.js', array(), true);
		wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/dist/js/main.min.js', array(), true);
	}
}

add_action('wp_enqueue_scripts', 'assets');

function setup_base()
{
	register_nav_menus(array(
		'primary' => __('Primary Menu', 'base'),
	));
	show_admin_bar(false);
	define('BLOG_URL', get_bloginfo("url"));
	define('TEMPLATE_URL', get_bloginfo("template_directory"));
	define('IMAGES_URL', get_bloginfo("template_directory") . "/assets/dist/img/");
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
	remove_image_size('medium_large');
}
add_action('after_setup_theme', 'setup_base');


function my_init()
{
	wp_enqueue_script('plugins', get_template_directory_uri() . '/assets/dist/js/plugins.js', array(), true);
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', false);
	}
}
add_action('init', 'my_init');

/*===================================================================================
* EXCERPT
* =================================================================================*/
function wpdocs_custom_excerpt_length($length)
{
	return 40;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);
function custom_excerpt_more($more)
{
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');

function get_excerpt($post_id, $num)
{
	$limit = $num + 1;
	$excerpt = get_words(get_the_excerpt($post_id), $limit);
	return $excerpt;
}

function get_words($text, $limit = 16)
{
	$excerptArr = explode(' ', $text, $limit);
	array_pop($excerptArr);
	$excerpt = implode(" ", $excerptArr);
	if (count($excerptArr) >= $limit - 1)
		$excerpt .= '...';
	return $excerpt;
}

function remove_head_scripts()
{
	remove_action('wp_head', 'wp_print_scripts');
	remove_action('wp_head', 'wp_print_head_scripts', 9);
	remove_action('wp_head', 'wp_enqueue_scripts', 1);

	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'remove_head_scripts');


/*===================================================================================
* PAGINATION
* =================================================================================*/
function the_pagination()
{
	global $wp_query;
	$big = 999999999; // need an unlikely integer
	echo paginate_links(array(
		'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
		'format' => '?paged=%#%',
		'prev_text' => '<',
		'next_text' => '>',
		'current' => max(1, get_query_var('paged')),
		'total' => $wp_query->max_num_pages
	));
}

/*===================================================================================
* GET YOAST SOCIAL VALUES
* =================================================================================*/
function get_yoast_social($field)
{
	$options = get_option('wpseo_social');
	return $options[$field];
}
/*===================================================================================
* DEBUG VARIABLES
* =================================================================================*/
function debug($content)
{
	echo '<pre class="pl-menu">';
	var_dump($content);
	echo '</pre>';
}
/*===================================================================================
* LIST TAXONOMIES
* =================================================================================*/
function get_list_taxonomy($post_id, $taxonomy, $show_url = true, $separator = ", ", $style = "")
{
	$terms = wp_get_post_terms($post_id, $taxonomy);
	$count = count($terms);
	$c = 1;
	$str = "";
	if ($terms) :
		foreach ($terms as $term) :
			$url = ($show_url) ? get_term_link($term->term_id) : 'javascript:void(0);';
			$str .= "<a class='$style' data-slug='$term->slug' data-taxonomy='$taxonomy' href='$url'>$term->name</a>";
			if ($separator && $count && $c < $count)
				$str .= $separator;
			$c++;
		endforeach;
	endif;
	return $str;
}

function the_acf_icon($file)
{
	$url = TEMPLATE_URL . "/assets/acf/$file.svg";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
	$svg = curl_exec($ch);
	curl_close($ch);
	echo $svg;
}

function getYoutubeIdFromUrl($url)
{
	$parts = parse_url($url);
	if (isset($parts['query'])) {
		parse_str($parts['query'], $qs);
		if (isset($qs['v'])) {
			return $qs['v'];
		} else if (isset($qs['vi'])) {
			return $qs['vi'];
		}
	}
	if (isset($parts['path'])) {
		$path = explode('/', trim($parts['path'], '/'));
		return $path[count($path) - 1];
	}
	return false;
}

function onlyNumbers($text)
{
	return preg_replace('/\D+/', '', $text);
}

function the_blocks()
{
	$blocks = get_field('blocos', get_the_ID());
	if ($blocks) :
		foreach ($blocks as $block) :
			$block_type = $block['acf_fc_layout'];
			if ($block_type == 'bloco_titulo_texto') :
				include 'includes/blocks/titulo-texto.php';
			elseif ($block_type == 'bloco_titulo_texto_imagem') :
				include 'includes/blocks/titulo-texto-imagem.php';
			elseif ($block_type == 'banner_contato') :
				include 'includes/blocks/banner-contato.php';
			elseif ($block_type == 'banner_generico') :
				include 'includes/blocks/banner-contato.php';
			elseif ($block_type == 'produtos_servicos') :
				include 'includes/blocks/produtos-servicos.php';
			elseif ($block_type == 'porque_contratar') :
				include 'includes/blocks/porque-contratar.php';
			elseif ($block_type == 'cases') :
				include 'includes/blocks/cases.php';
			elseif ($block_type == 'video') :
				include 'includes/blocks/video.php';
			else :
				include 'includes/blocks/simple.php';
			endif;

		endforeach;
	endif;
}

function the_image($filename, $alt = '', $mobile = false, $retina = false)
{
	$url = (strpos($filename, 'ttp')) ? $filename : IMAGES_URL . $filename;
	$url_mb = IMAGES_URL . str_replace('.', '@mb.', $filename);
	$url_2x = IMAGES_URL . str_replace('.', '@2x.', $filename);

	$tag = "<picture>";
	if ($mobile)
		$tag .= 	"<source srcset='$url_mb' media='(max-width: 500px)' >";
	if ($retina)
		$tag .= 	"<source srcset='$url_2x 2x' >";

	$tag .= 	"<source srcset='$url'>
	<img srcset='$url' alt='$alt' >";
	$tag .=  "</picture>";
	echo $tag;
}

/*===================================================================================
*  INCLUDES
* =================================================================================*/
include "functions/global-config.php";
include "functions/uniforms.php";
