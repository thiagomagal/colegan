<?php get_header(); ?>

<?php

if (have_posts()) : the_post();
	$image = get_field('banner_imagem');
?>

	<section class="highlight highlight-page small" <?php if ($image) : ?> style="background-image:url(<?php echo $image['url'] ?>);" <?php endif; ?>>
		<div class="container h-100">
			<div class="row h-100">
				<div class="col-md-10 align-self-center">
					<h1><?php the_title(); ?></h1>
					<p><?php the_field('banner_descricao'); ?></p>
				</div>
			</div>
		</div>
	</section>

	<section class="py-5 post-single ">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 offset-md-1 align-self-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

	<?php the_blocks(); ?>

<?php endif; ?>

<?php get_footer(); ?>