<?php get_header();
$image = get_the_post_thumbnail_url($uniform->ID, 'large'); ?>

<section class="highlight highlight-small" <?php if ($image) : ?> style="background-image:url(<?php echo $image; ?>);" <?php endif; ?>>
  <div class="container h-100">
    <div class="row h-100">
      <div class="col-12 align-self-center text-center">
        <h1 class="color-white text-center">Uniformes <?php the_title(); ?></h1>
        <p><?php the_field('banner_descricao'); ?></p>
      </div>
    </div>
  </div>
</section>

<?php $content = get_the_content();
if ($content) : ?>
  <section class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <?php echo $content; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php $items = get_field('itens');
if ($items) : ?>
  <section class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="row">
            <?php foreach ($items as $item) : ?>
              <div class="col-12 mb-5 uniform-item">
                <div class="row">
                  <div class="col-auto wow fadeInLeft">
                    <div class="uniform-item__icon">
                      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="405.272px" height="405.272px" viewBox="0 0 405.272 405.272" style="enable-background:new 0 0 405.272 405.272;" xml:space="preserve">
                        <g>
                          <path d="M393.401,124.425L179.603,338.208c-15.832,15.835-41.514,15.835-57.361,0L11.878,227.836 c-15.838-15.835-15.838-41.52,0-57.358c15.841-15.841,41.521-15.841,57.355-0.006l81.698,81.699L336.037,67.064 c15.841-15.841,41.523-15.829,57.358,0C409.23,82.902,409.23,108.578,393.401,124.425z" />
                        </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                        <g> </g>
                      </svg>
                    </div>
                  </div>
                  <div class="col align-self-center wow fadeInRight">
                    <h3><?php echo ucwords(strtolower($item['nome'])); ?></h3>
                    <p><?php echo $item['descricao']; ?></p>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>


<?php $items = get_field('lista_em_box');
if ($items['itens']) : ?>
  <section class="box-items">
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <h2 class="text-center mb-5"><?php echo $items['titulo'] ?></h2>
          <div class="row justify-content-center">
            <?php $c = 1;
            foreach ($items['itens'] as $item) : ?>
              <div class="col-md-4  mb-2 mb-md-4 text-center wow fadeInUp" data-wow-delay="0.<?php echo $c; ?>s">
                <div class="box-items__item">
                  <div class="row h-100">
                    <div class="col-12 align-self-center">
                      <p class="mb-0"><?php echo $item['texto_inicial']; ?></p>
                      <h3><?php echo $item['texto']; ?></h3>
                    </div>
                  </div>
                </div>
              </div>
            <?php ($c <= 3) ? $c++ : $c = 1;
            endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<?php if (get_field('ocultar_orcamento')) : ?>

  <section class="bg-color-1 color-white py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1 text-center">
          <h2>Para mais informações sobre o seu uniforme, entre em contato pelo número (31) 3452-0671</h2>
          <h3>ou mande uma mensagem para o nosso whatsapp</h3>
          <a class="btn white" target="_blank" href="https://web.whatsapp.com/send?phone=553134520671">Enviar mensagem</a>
        </div>
      </div>
    </div>
  </section>

<?php else : ?>

  <?php get_template_part('includes/budget', 'budget') ?>

<?php endif; ?>

<?php get_footer(); ?>