<?php get_header(); ?>

<?php $highlights = get_field('destaques');  ?>
<section id="inicio" class="highlights bg-color-1">
  <div class="highlights-images owl-theme owl-carousel">
    <?php foreach ($highlights as $highlight) : ?>
      <div class="highlight" style="background-image:url(<?php echo $highlight['imagem']; ?>)">
        <div class="highlight-content">
          <div class="container h-100">
            <div class="row h-100">
              <div class="col-md-auto align-self-end highlight__content">
                <h2><?php echo $highlight['titulo'] ?></h2>
                <h3><?php echo $highlight['descricao'] ?></h3>
                <?php if ($highlight['texto_botao']) : ?>
                  <a class="btn white" class="scrollTo" href="<?php echo $highlight['texto_botao']; ?>"><?php echo $highlight['texto_botao'] ?></a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</section>

<?php get_template_part('includes/why', 'why') ?>

<?php get_template_part('includes/uniforms-segments', 'uniforms-segments') ?>

<?php get_template_part('includes/budget', 'budget') ?>

<?php get_footer(); ?>