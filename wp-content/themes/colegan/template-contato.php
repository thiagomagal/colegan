<?php /* Template Name: Contato */ ?>

<?php get_header();
?>

<section class="py-5 text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</section>

<section class="contact bg-color-black color-white pb-5">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="contact-arrow"></div>
      </div>
      <div class="col-md-8 offset-md-2">
        <?php echo do_shortcode('[contact-form-7 title="Contato"]') ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>