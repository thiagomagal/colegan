<?php

function codex_uniforms_init()
{

	$labels = array(
		'name'               => _x('Uniforme', 'post type general name', 'your-plugin-textdomain'),
		'singular_name'      => _x('Uniforme', 'post type singular name', 'your-plugin-textdomain'),
		'menu_name'          => _x('Uniformes', 'admin menu', 'your-plugin-textdomain'),
		'name_admin_bar'     => _x('Uniforme', 'add new on admin bar', 'your-plugin-textdomain'),
		'add_new'            => _x('Adicionar Uniforme', 'curso', 'your-plugin-textdomain'),
		'add_new_item'       => __('Novo Uniforme', 'your-plugin-textdomain'),
		'new_item'           => __('Novo Uniforme', 'your-plugin-textdomain'),
		'edit_item'          => __('Editar Uniforme', 'your-plugin-textdomain'),
		'view_item'          => __('Ver Uniforme', 'your-plugin-textdomain'),
		'all_items'          => __('Todos Uniformes', 'your-plugin-textdomain'),
		'search_items'       => __('Buscar Uniforme', 'your-plugin-textdomain'),
		'parent_item_colon'  => __('Parent Uniforme:', 'your-plugin-textdomain'),
		'not_found'          => __('Nenhum Uniforme encontrados.', 'your-plugin-textdomain'),
		'not_found_in_trash' => __('Nenhum Uniforme na lixeira.', 'your-plugin-textdomain')
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __('Description.', 'your-plugin-textdomain'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_admin_column' => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title', 'editor', 'thumbnail'),
		'show_in_rest'		 => true,
		'taxonomies'          => array(),
		'rewrite' => array('slug' => 'uniformes')
	);

	register_post_type('uniform', $args);
}

add_action('init', 'codex_uniforms_init');


add_action('pre_get_posts', 'my_change_sort_order');
function my_change_sort_order($query)
{
	if (is_archive() && $query->query_vars['post_type'] == 'case') :
		$query->set('order', 'ASC');
		$query->set('posts_per_page', -1);
	endif;
};
