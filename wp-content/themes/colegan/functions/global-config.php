<?php

if (function_exists('acf_add_options_page')) {

	acf_add_options_page(array(
		'page_title' 	=> 'Conteúdos Gerais',
		'menu_title'	=> 'Conteúdos Gerais',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

add_filter(
	'acf_icon_path_suffix',
	function ($path_suffix) {
		return 'assets/acf/'; // After assets folder you can define folder structure
	}
);
