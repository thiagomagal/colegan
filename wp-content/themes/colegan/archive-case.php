<?php get_header(); ?>

<section class="py-5 text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <h1>Onde você vê desafios, nós enxergamos potencial de resultados</h1>
      </div>
    </div>
  </div>
</section>

<?php
$c = 1;
if (have_posts()) :
  while (have_posts()) : the_post();
?>

    <section>
      <div class="container">
        <div class="row">
          <?php if ($logo = get_field('logo')) : ?>
            <div class="col-12 case-logo text-center">
              <img src="<?php echo $logo; ?>" alt="<?php the_title(); ?>" />
            </div>
          <?php endif; ?>
        </div>
        <?php if ($blocks = get_field('blocos')) : foreach ($blocks as $block) : ?>
            <div class="row">
              <div class="col-md-4 text-md-end pe-md-5">
                <h2><?php echo $block['titulo']; ?></h2>
              </div>
              <div class="col-md pb-5 ps-md-5">
                <?php echo $block['conteudo']; ?>
              </div>
            </div>
        <?php endforeach;
        endif; ?>
      </div>
    </section>

    <?php $numbers = get_field('numeros');
    if ($numbers) :  ?>

      <section class="case-numbers">
        <div class="container">
          <div class="row">
            <?php $c = 1;
            foreach ($numbers as $number) : ?>
              <div class="col-md case-number text-center wow fadeInUp" data-wow-delay="0.<?php echo $c; ?>s">
                <div class="case-numbers__icon">
                  <?php the_acf_icon($number['icone']); ?>
                </div>
                <h3><?php echo $number['numero'] ?></h3>
                <p><?php echo $number['texto'] ?></p>
              </div>
            <?php $c++;
            endforeach; ?>
          </div>
        </div>
      </section>

    <?php endif; ?>

<?php endwhile;
endif; ?>


<?php get_template_part('includes/lets-talk', 'lets-talk') ?>

<?php get_footer(); ?>