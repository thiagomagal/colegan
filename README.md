# Colegan

## Dependências

- PHP Mysql
- Composer (https://getcomposer.org/download/)
- npm
- sass

---

## Iniciando o projeto

- Execute o comando `$ composer install` na raiz do projeto
- Copie o conteúdo do arquivo wp-config-sample.php e cole em um novo arquivo wp-config.php
- Configure o arquivo wp-config de acordo com suas credendiais Mysql

_Os plugins do Wordpress são instalado pelo composer. Para adicionar um plugin altere o arquivo composer.json na raiz do projeto e execute o comando `$ composer install`._

## Para editar o front-end

- Execute o comando `$ npm install` no diretório wp-content/themes/colegan/assets
- Execute o comando `$ gulp` no diretório wp-content/themes/colegan/assets
